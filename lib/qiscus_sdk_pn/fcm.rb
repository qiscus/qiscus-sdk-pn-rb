# frozen_string_literal: true
require "fcm"

module QiscusSdkPn
  #
  # Send FCM to Firebase via HTTP protocol.
  # For more info, please see https://s.id/oRAYs
  #
  class Fcm
    #
    # Initialize FCM
    #
    # @param [String] key FCM server key
    #
    def initialize(fcm_key:)
      @key = fcm_key
      @fcm = FCM.new(fcm_key)
    end

    #
    # Send FCM using available Qiscus SDK webhook payload
    #
    # @param [Array] tokens Array of user device token
    # @param [Integer] qiscus_room_id Room ID in Qiscus SDK
    # @param [Hash] pn_payload FCM payload that compatible with Qiscus Flash
    #
    # @return [Symbol] :ok
    #
    def send!(tokens, pn_payload)
      data = {}
      data["payload"] = pn_payload
      data["qiscus_room_id"] = pn_payload["room_id"]
      data["qiscus_sdk"] = "post_comment"

      options = {}
      options["priority"] = "high"
      options["data"] = data

      res = @fcm.send(tokens, options)
      catch_error(res)

      :ok
    end

    #
    # Send FCM with custom options. This method only implement
    # Available method in https://github.com/spacialdb/fcm
    #
    # @param [Array] tokens Array of user device token
    # @param [Hash] options FCM options
    #
    # @return [Symbol] :ok
    #
    def send(tokens, options = {})
      res = @fcm.send(tokens, options)
      catch_error(res)

      :ok
    end

    private

    def catch_error(res)
      code = res[:status_code]
      unless code == 200
        response = res[:response]
        raise FcmError, response
      end
    end
  end
end
