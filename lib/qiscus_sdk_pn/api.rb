# frozen_string_literal: true
require "http"

module QiscusSdkPn
  class Api
    #
    # Initialize Qiscus SDK Rest API
    #
    # @param [String] qiscus_app_code App code/App ID in Qiscus SDK app
    # @param [String] qiscus_secret_key Secret key in Qiscus SDK app
    # @param [Hash] options Additional options
    # @option options [String] :base_url You can change Qiscus SDK Base URL. Example: "https://api3.qiscus.com/api/v2.1/rest"
    #
    def initialize(qiscus_app_code, qiscus_secret_key, options = {})
      @headers = {}
      @headers["QISCUS_SDK_APP_ID"] = qiscus_app_code
      @headers["QISCUS_SDK_SECRET"] = qiscus_secret_key

      @base_url = "https://api.qiscus.com/api/v2.1/rest"

      options.each do |key, value|
        instance_variable_set("@#{key}", value)
      end
    end

    #
    # Get total messages that has not seen by user
    #
    # @param [String] user_ids User id in Qiscus SDK app
    #
    # @return [Hash] Unread count
    #
    def get_user_unread_count(user_ids)
      url = "#{@base_url}/get_user_unread_count"
      user_ids = user_ids.is_a?(Array) ? user_ids : [user_ids]
      user_ids = transform_to_query("user_ids", user_ids)

      res = HTTP.get(url, headers: @headers, params: user_ids)
      catch_error(res.status, res.body.to_s) unless res.status.success?

      data = JSON.parse(res.body.to_s)
      data["results"]["user_unread_counts"]
    end

    private

    def transform_to_query(key, value)
      value = value.is_a?(Array) ? value : [value]
      key = key.end_with?("[]") ? key : "#{key}[]"
      query = []

      value.each { |v| query.push([key, v]) }

      query
    end

    def catch_error(code, body_str)
      base_message = "Request to Qiscus SDK failed"

      if code == 400
        body = JSON.parse(body_str)
        error_message = body["error"]["message"]

        raise BadRequest, "#{base_message}: #{error_message}"
      end

      if code == 403
        raise Forbidden, "#{base_message}: Wrong secret key"
      end

      if code >= 500
        raise InternalServerError, "#{base_message}: Internal server error"
      end
    end
  end
end
