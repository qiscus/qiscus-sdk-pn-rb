# frozen_string_literal: true
require "openssl"
require "apnotic"

module QiscusSdkPn
  class Apns
    def initialize(cert_path:, cert_pass: "", cert_topic:, is_prod: true)
      @cert_path = cert_path
      @cert_pass = cert_pass
      @cert_topic = cert_topic
      @is_prod = is_prod
    end

    def send!(token, pn_payload, pn_aps)
      pn_payload["qiscus_sdk"] = "post_comment"
      pn_payload["qiscus_room_id"] = pn_payload["room_id"]

      notification = Apnotic::Notification.new(token)
      notification.topic = @cert_topic
      notification.alert = pn_aps["alert"]
      notification.content_available = pn_aps["content-available"]
      notification.mutable_content = pn_aps["mutable-content"]
      notification.sound = pn_aps["sound"]
      notification.badge = pn_aps["badge"]
      notification.custom_payload = pn_payload

      response = conn.push(notification)
      catch_error(response)

      :ok
    end

    private

    def conn
      if @is_prod
        Apnotic::Connection.new(cert_path: @cert_path, cert_pass: @cert_pass)
      else
        Apnotic::Connection.development(cert_path: @cert_path, cert_pass: @cert_pass)
      end
    end

    def get_topic
      pkcs = OpenSSL::PKCS12.new(File.read(@cert_path), @cert_pass)
      subject = pkcs.certificate.subject.inspect
      stringify_subject = subject.delete_prefix("#<OpenSSL::X509::Name ").delete_suffix(">")
      uid = stringify_subject.split(",").select { |s| s.start_with?("UID=") }.first
      topic = uid.delete_prefix("UID=")

      topic
    rescue => error
      raise InvalidCertOrPass, error.message
    end

    def catch_error(response)
      base_message = "Sending APNS PN failed:"
      code = response.status.to_i

      if code == 502 || code > 503
        msg = "#{base_message}: Unexpected server error"
        raise UnexpectedServerError, msg
      end

      if code == 500
        msg = "#{base_message}: Internal server error"
        raise InternalServerError, msg
      end

      if code == 503
        msg = "#{base_message}: The server is shutting down and unavailable"
        raise ServerUnavailable, msg
      end

      if code == 429
        msg = "#{base_message}: The server received too many requests for the same device token"
        raise TooManyRequest, msg
      end

      if code == 413
        msg = "#{base_message}: The notification payload was too large"
        raise PayloadTooLarge, msg
      end

      if code == 410
        msg = "#{base_message}: The device token is no longer active for the topic"
        raise DeviceTokenNotActive, msg
      end

      if code == 405
        msg = "#{base_message}: The request used an invalid :method value. Only POST requests are supported"
        raise MethodNotAllowed, msg
      end

      if code == 403
        msg = "#{base_message}: There was an error with the certificate or with the provider’s authentication token"
        raise InvalidCertificate, msg
      end

      if code == 400
        reason = response.body["reason"]
        msg = "#{base_message}: #{reason}"
        raise BadRequest, msg
      end
    end
  end
end
