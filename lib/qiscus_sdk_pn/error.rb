# frozen_string_literal: true
module QiscusSdkPn
  class UnexpectedServerError < StandardError; end
  class InternalServerError < StandardError; end
  class ServerUnavailable < StandardError; end
  class TooManyRequest < StandardError; end
  class PayloadTooLarge < StandardError; end
  class DeviceTokenNotActive < StandardError; end
  class MethodNotAllowed < StandardError; end
  class InvalidCertificate < StandardError; end
  class BadRequest < StandardError; end
  class RequestTimeout < StandardError; end
  class InvalidCertOrPass < StandardError; end

  class InvalidServerKey < StandardError; end
  class FcmError < StandardError; end

  class Unauthorized < StandardError; end
  class Forbidden < StandardError; end
end
