# frozen_string_literal: true

module QiscusSdkPn
  class Payload
    #
    # Initialize payload
    #
    # @param [Hash] webhook_data Full payload from Qiscus SDK webhook
    #
    def initialize(webhook_data)
      @data = webhook_data
    end

    def payload
      @data["payload"]
    end

    def user
      payload["from"]
    end

    alias_method(:sender, :user)
    alias_method(:from, :user)

    def room
      payload["room"]
    end

    def room_id
      payload["room"]["id"]
    end

    def room_type
      room["type"]
    end

    def username
      user["name"]
    end

    def qiscus_room_id
      room["id"]
    end

    def message
      payload["message"]
    end

    def message_type
      message["type"]
    end

    def file_url
      if message_type == "file_attachment"
        message["payload"]["url"]
      end
    end

    def message_text
      message["text"]
    end

    #
    # Fetch PN recipients from Qiscus SDK webhook payload
    #
    # @return [Array] List of user id in Qiscus SDK app
    #
    def fetch_pn_recipients
      sender_email = user["email"]
      participants = room["participants"]

      if participants.blank?
        participants_data = nil
      else
        participants_data = []
        participants.each do |p|
          participants_data.push(p["email"]) unless p["email"] == sender_email
        end
      end

      participants_data
    end

    #
    # Build payload that compatible with Qiscus SDK Flash
    #
    # @return [Hash] payload
    #
    def build_pn_payload
      payload = {}
      payload["comment_before_id"] = message["comment_before_id"]
      payload["comment_before_id_str"] = message["comment_before_id_str"]
      payload["email"] = user["email"]
      payload["extras"] = message["extras"]
      payload["id"] = message["id"]
      payload["id_str"] = message["id_str"]
      payload["is_public_channel"] = room["is_public_channel"]
      payload["message"] = message_text
      payload["payload"] = message["payload"]
      payload["room_avatar"] = room["room_avatar"]
      payload["room_id"] = room["id"]
      payload["room_id_str"] = room["id_str"]
      payload["room_name"] = room["name"]
      payload["room_options"] = room["options"]
      payload["room_type"] = room["type"]
      payload["chat_type"] = room["type"]
      payload["status"] = "sent"
      payload["timestamp"] = message["timestamp"]
      payload["type"] = message_type
      payload["unique_temp_id"] = message["unique_temp_id"]
      payload["unix_nano_timestamp"] = message["unix_nano_timestamp"]
      payload["unix_timestamp"] = message["unix_timestamp"]
      payload["user_avatar"] = user["avatar_url"]
      payload["username"] = username

      payload
    end

    #
    # Build "aps" payload for Apns notification
    #
    # @param [String] qiscus_user_id Id of user in Qiscus SDK
    # @param [String] qiscus_app_code App code/App ID in Qiscus SDK app
    # @param [String] qiscus_secret_key Secret key of Qiscus SDK app
    #
    # @return [Hash] aps hash
    #
    def build_pn_aps(qiscus_user_id, qiscus_app_code, qiscus_secret_key)
      aps = {}
      aps["content-available"] = 1
      aps["mutable-content"] = 1
      aps["sound"] = "default"

      sdk = QiscusSdkPn::Api.new(qiscus_app_code, qiscus_secret_key)
      data = sdk.get_user_unread_count(qiscus_user_id)

      data = data.first
      unread_count = data["unread_count"]

      aps["badge"] = unread_count
      aps["alert"] = build_aps_alert

      aps
    end

    private

    def build_aps_alert
      alert = "New message."
      alert = build_aps_alert_text if message_type == "text"
      alert = build_aps_alert_file if message_type == "file_attachment"
      alert
    end

    def build_aps_alert_text
      alert = "New message."

      if has_file_inside_text?(message_text)
        url = fetch_file_url_from_text(message_text)
        alert = file_url_alert(url)
      else
        alert = message_text
      end

      alert = "#{username}: #{alert}" unless room_type == "single"
      alert
    end

    def build_aps_alert_file
      alert = file_url_alert(file_url)

      if room_type != "single"
        alert = "#{username}: #{alert}"
      end

      alert
    end

    def has_file_inside_text?(text)
      text.start_with?("[file]") && text.end_with?("[/file]")
    end

    def fetch_file_url_from_text(text)
      text.strip.delete_prefix("[file]").delete_suffix("[/file]").strip
    end

    def file_url_alert(url)
      ext = File.extname(url)

      alert = "Sent a file."

      image_ext = [".jpg", ".jpeg", ".png", ".gif", ".tiff", ".svg"]
      if image_ext.include?(ext)
        alert = "Sent an image."
      end

      video_ext = [".mp4", ".mov", ".avi", ".mpg", ".mpeg", ".mkv"]
      if video_ext.include?(ext)
        alert = "Sent a video."
      end

      document_ext = [".pdf", ".doc", ".docx", ".odt", ".ppt", ".pptx", ".xls", ".xlsx", ".odp", ".ods"]
      if document_ext.include?(ext)
        alert = "Sent a document."
      end

      alert.strip
    end
  end
end
