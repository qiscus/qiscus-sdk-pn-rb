# frozen_string_literal: true
require "mqtt"

module QiscusSdkPn
  class Mqtt
    def initialize(url: "mqtt://mqtt.qiscus.com", port: 1883, ssl: false, qos: 0)
      @url = url
      @port = port
      @ssl = ssl
      @qos = qos
    end

    def publish(topic, payload: {}, retain: false)
      mqtt_client.publish(topic, payload, retain)

      [:ok, nil]
    rescue StandardError => e
      [:error, e.message]
    end

    alias_method :pub, :publish

    def publish!(topic, payload: {}, retain: false)
      mqtt_client.publish(topic, payload, retain)

      :ok
    end

    private

    def mqtt_client
      @client ||= MQTT::Client.connect(@url, port: @port, ssl: @ssl, will_qos: @qos)
      @client
    end
  end
end