# frozen_string_literal: true
module QiscusSdkPn
  class MqttPayload
    def initialize(app_id:, params:)
      @app_id = app_id
      params = JSON.parse(params.to_json, symbolize_names: true)
      @sender = params.dig(:payload, :from)
      @room = params.dig(:payload, :room)
      @message = params.dig(:payload, :message)
    end

    def build_new_comment_payload
      {
        app_code: @app_id,
        chat_type: @room.dig(:type),
        comment_before_id: @message.dig(:comment_before_id),
        comment_before_id_str: @message.dig(:comment_before_id_str),
        created_at: @message.dig(:created_at),
        disable_link_preview: @message.dig(:disable_link_preview),
        email: @sender.dig(:email),
        extras: @message.dig(:extras),
        id: @message.dig(:id),
        id_str: @message.dig(:id_str),
        is_public_channel: @room.dig(:is_public_channel),
        message: @message.dig(:text),
        payload: @message.dig(:payload),
        raw_room_name: @room.dig(:name),
        room_avatar: @room.dig(:room_avatar),
        room_id: @room.dig(:id),
        room_id_str: @room.dig(:id_str),
        room_name: @room.dig(:name),
        room_options: @room.dig(:options),
        room_type: @room.dig(:options),
        status: "sent", # set default to "sent" since Qiscus SDK not sent message status in webhook payload
        timestamp: @message.dig(:timestamp),
        topic_id: @room.dig(:topic_id),
        topic_id_str: @room.dig(:topic_id_str),
        type: @message.dig(:topic_id_str),
        unique_temp_id: @message.dig(:unique_temp_id),
        unix_nano_timestamp: @message.dig(:unix_nano_timestamp),
        unix_timestamp: @message.dig(:unix_timestamp),
        user_avatar: @sender.dig(:avatar_url),
        user_avatar_url: @sender.dig(:avatar_url),
        user_extras: {}, # set blank hash since Qiscus SDK not sent user extras in webhook payload
        user_id: @sender.dig(:id),
        user_id_str: @sender.dig(:id_str),
        username: @sender.dig(:name),
      }
    end
  end
end