# Qiscus SDK PN Ruby

Ruby library to send push notifications to FCM, APNS, or Pushkit. This library is compatible with PN payload sent by the Qiscus Flash service

## Installation

Add this line to your application's Gemfile:

```ruby
gem "qiscus_sdk_pn", git: "https://bitbucket.org/qiscus/qiscus-sdk-pn-rb"
```

And then execute:

```ruby
bundle install
```

## Quickstart

**Send push notification to FCM**

```ruby
webhook = QiscusSdkPn::Payload.new(params) # params = Qiscus SDK webhook full payload
pn_payload = webhook.build_pn_payload

QiscusSdkPn::Fcm.new(fcm_key: "12345678").send!(
  ["registration_id_1", "registration_id_2"],
  pn_payload
)
```

**Send push notification to APNS**

```ruby
webhook = QiscusSdkPn::Payload.new(params) # params = Qiscus SDK webhook full payload
pn_payload = webhook.build_pn_payload
pn_aps = webhook.build_pn_aps(
  "user@example.com", # user email in Qiscus SDK 
  "12345678", # Qiscus SDK app code
  "87654321" # Qiscus SDK secret key
)

QiscusSdkPn::Apns.new(
  cert_path: "#{Rails.root}/app/assets/ios_cert/production/ios-pn-prod.p12",
  cert_topic: "com.qiscus.us",
  cert_pass: "12345678",
  is_prod: true
).send!(token, pn_payload, pn_aps)
```

**To find out the PN recipients, you can also run this code**

```ruby
payload = QiscusSdkPn::Payload.new(params)
recipient_emails = payload.fetch_pn_recipients
puts recipient_emails # ["12345678", "87654321"]
```

**Publish to Qiscus MQTT**

You can also build the payload that compatible with Qiscus MQTT `new_comment` topic and publish the message through this package. You can use this mechanism when you want the list message that contain room with channel type (Qiscus SDK not published `new_comment` event this room type).

>  Note: Please only use this for room with type channel

```ruby
payload = QiscusSdkPn::MqttPayload.new(app_id: 'YOUR_APP_ID', params: 'FULL_WEBHOOK_PARAMS').build_new_comment_payload
mqtt = QiscusSdkPn::Mqtt.new

participants = ChatUser.includes(:user).where(room_id: YOUR_ROOM_ID)
participants.each { |p| mqtt.publish("#{p.user.qiscus_token}/c", payload.to_json) } # better if this code block sent to background job
```